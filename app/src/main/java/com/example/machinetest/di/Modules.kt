package com.example.machinetest.di
import com.example.machinetest.data.network.ApiHelper
import com.example.machinetest.data.network.ApiService
import com.example.machinetest.data.network.ApiServiceImpl
import com.example.machinetest.data.repository.ReposRepo
import com.example.machinetest.ui.detail.DetailViewModel
import com.example.machinetest.ui.main.MainViewModel
import com.example.machinetest.ui.main.ReposAdapter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module



val NetworkModule = module {
    factory<ApiService> { (ApiServiceImpl()) }
    factory { ApiHelper(apiService = get()) }
}

val FactoryModule = module {
    single { ReposRepo(apiHelper= get()) }
}

val ViewModelModule = module {
    viewModel { MainViewModel(reposRepo = get()) }
    viewModel { DetailViewModel() }
}

val AdapterModule = module {
    factory { ReposAdapter() }

}

val appModules = listOf(NetworkModule, FactoryModule, ViewModelModule, AdapterModule)