package com.example.machinetest.utils

import android.view.View
import com.example.machinetest.ui.base.BaseViewHolder

class EmptyViewHolder internal constructor(itemView: View) : BaseViewHolder(itemView) {

    override fun clear() {

    }

}