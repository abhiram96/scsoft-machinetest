package com.example.machinetest.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    EXCEPTION
}