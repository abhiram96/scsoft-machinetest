package com.example.machinetest

import android.app.Application
import com.example.machinetest.di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MachineApp : Application() {
    companion object {
        private lateinit var instance: MachineApp
        fun get(): MachineApp = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin {
            androidContext(this@MachineApp)
            // your modules
            modules(appModules)
        }
    }
}
