package com.example.machinetest.data.network

class ApiHelper(private val apiService: ApiService) {

    fun getReposApiCall() = apiService.getReposApiCall()
}