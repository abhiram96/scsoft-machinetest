package com.example.machinetest.data.model

import com.google.gson.annotations.SerializedName

data class ReposResponse(
    val name: String?,
    val id: String?,

    @SerializedName("full_name")
    val fullName: String?,

    val description: String?,

    @SerializedName("watchers_count")
    val watchersCount: String?,

    @SerializedName("stargazers_count")
    val stargazersCount: String?,

    @SerializedName("open_issues_count")
    val openIssuesCount: String?,

    val visibility: String?,

    )