package com.example.machinetest.data.repository
import com.example.machinetest.data.model.ReposResponse
import com.example.machinetest.data.network.ApiHelper
import io.reactivex.Single


class ReposRepo(private val apiHelper: ApiHelper) {
    fun getApodsApiCall(): Single<ArrayList<ReposResponse>> {
        return apiHelper.getReposApiCall()
    }


}