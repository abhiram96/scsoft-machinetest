package com.example.machinetest.data.network

import com.example.machinetest.data.model.ReposResponse
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {
    @GET(ApiEndPoints.repos)
    fun getReposApiCall(): Single<ArrayList<ReposResponse>>
}