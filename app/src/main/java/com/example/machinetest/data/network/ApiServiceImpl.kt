package com.example.machinetest.data.network

import com.example.machinetest.data.model.ReposResponse
import io.reactivex.Single

class ApiServiceImpl : ApiService {

    override fun getReposApiCall(): Single<ArrayList<ReposResponse>> {
        return ApiClient.instance.getReposApiCall()
    }
}