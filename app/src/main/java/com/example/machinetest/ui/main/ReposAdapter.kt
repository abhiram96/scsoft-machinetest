package com.example.machinetest.ui.main
import android.content.Context
import android.content.Intent
import com.example.machinetest.ui.base.BaseViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.machinetest.R
import com.example.machinetest.data.model.ReposResponse
import com.example.machinetest.ui.detail.DetailActivity
import com.example.machinetest.utils.AppConstant
import com.example.machinetest.utils.EmptyViewHolder


class ReposAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    private val category = ArrayList<ReposResponse>()
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            AppConstant.VIEW_TYPE_NORMAL -> ViewHolder(
                (LayoutInflater.from(parent.context)
                        .inflate(R.layout.row_repos, parent, false))
            )
            AppConstant.VIEW_TYPE_EMPTY -> EmptyViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_empty_view, parent, false)
            )
            else -> EmptyViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_empty_view,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return if (category.size > 0) {
            category.size
        } else {
            1
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (category.isNotEmpty()) {
            AppConstant.VIEW_TYPE_NORMAL
        } else {
            AppConstant.VIEW_TYPE_EMPTY
        }
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {

        private var layout = itemView.findViewById<LinearLayout>(R.id.details_layout)
        private var name = itemView.findViewById<TextView>(R.id.tv_name)
        private var fullName = itemView.findViewById<TextView>(R.id.tv_full_name)
        private var id = itemView.findViewById<TextView>(R.id.tv_id)


        override fun onBind(position: Int) {
            super.onBind(position)
            try {
                val item = category[position]
                name.text = "Name : - ${item.name}"
                fullName.text = "Full name : - ${item.fullName}"
                id.text = "Id : - ${item.id}"


                layout.setOnClickListener {
                    val intent = Intent(context, DetailActivity::class.java)
                    intent.putExtra("name", item.name)
                    intent.putExtra("fullName", item.fullName)
                    intent.putExtra("id", item.id)
                    intent.putExtra("description", item.description)
                    intent.putExtra("watchersCount", item.watchersCount)
                    intent.putExtra("stargazersCount", item.stargazersCount)
                    intent.putExtra("openIssuesCount", item.openIssuesCount)
                    intent.putExtra("visibility", item.visibility)
                    context?.startActivity(intent)
                }

            } catch (e: Exception){
                println("========================= ${e.message}")
            }
        }

        override fun clear() {

        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    fun addItems(list: ArrayList<ReposResponse>, context: Context) {
        category.clear()
        category.addAll(list)
        this.context = context
        notifyDataSetChanged()
    }
}