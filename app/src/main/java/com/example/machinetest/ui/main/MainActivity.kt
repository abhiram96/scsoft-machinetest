package com.example.machinetest.ui.main

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.machinetest.R
import com.example.machinetest.databinding.ActivityMainBinding
import com.example.machinetest.ui.base.BaseActivity
import com.example.machinetest.utils.Status
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    private val mainViewModel : MainViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding
    private val adapter: ReposAdapter by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.mainViewModel = mainViewModel
        binding.lifecycleOwner = this
        setUp()
    }
    override fun setUp() {
        title = "Repos Listing"
        binding.rvApod.adapter = adapter
        getApod()
    }
    private fun getApod() {
        mainViewModel.getResponse().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { response ->
                        hideProgress()
                        adapter.addItems(response, this)
                    }
                }
                Status.LOADING -> {
                    showProgress()

                }
                Status.ERROR -> {
                    //Handle Error
                    hideProgress()
                }
                Status.EXCEPTION -> {
                    hideProgress()

                }
            }
        })

    }
    override fun showProgress() {
        binding.rvApod.visibility = View.INVISIBLE
        binding.pbar.visibility = View.VISIBLE
    }
    override fun hideProgress() {
        binding.pbar.visibility = View.GONE
        binding.rvApod.visibility = View.VISIBLE
    }
}