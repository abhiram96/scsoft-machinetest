package com.example.machinetest.ui.main

import com.example.machinetest.utils.Resource
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.machinetest.data.model.ReposResponse
import com.example.machinetest.data.repository.ReposRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(private val reposRepo: ReposRepo) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    val reposResponse = MutableLiveData<Resource<ArrayList<ReposResponse>>>()

    init {
        getApod()
    }

    fun getApod(){
        reposResponse.postValue(Resource.loading(null))
        compositeDisposable.add(
            reposRepo.getApodsApiCall()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    reposResponse.postValue(Resource.success(response))
                }, {
                    reposResponse.postValue(Resource.exception(it.message))
                })
        )
    }

    fun getResponse() : MutableLiveData<Resource<ArrayList<ReposResponse>>> {
        return reposResponse
    }
    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}