package com.example.machinetest.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.machinetest.R
import com.example.machinetest.databinding.ActivityDetailBinding
import com.example.machinetest.databinding.ActivityMainBinding
import com.example.machinetest.ui.base.BaseActivity
import com.example.machinetest.ui.main.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailActivity : BaseActivity() {

    private val detailViewModel : DetailViewModel by viewModel()
    private lateinit var binding: ActivityDetailBinding
    private var name = ""
    private var fullName = ""
    private var id = ""
    private var description = ""
    private var watchersCount = ""
    private var stargazersCount = ""
    private var openIssuesCount = ""
    private var visibility = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.detailViewModel = detailViewModel
        binding.lifecycleOwner = this
        val data =  intent.extras
        name = data!!.getString("name")?:""
        fullName = data.getString("fullName")?:""
        id = data.getString("id")?:""
        description = data.getString("description")?:""
        watchersCount = data.getString("watchersCount")?:""
        stargazersCount = data.getString("stargazersCount")?:""
        openIssuesCount = data.getString("openIssuesCount")?:""
        visibility = data.getString("visibility")?:""
        setUp()
    }

    override fun setUp() {
        title = "Repos Details"
        binding.tvId.text = "Id : - ${id}"
        binding.tvName.text = "Name : - ${name}"
        binding.tvFullName.text = "Full name : - ${fullName}"
        binding.tvDescription.text = "Description : - ${description}"
        binding.tvStargazerCount.text = "Stargazers count : - ${stargazersCount}"
        binding.tvWatchersCount.text = "Watchers count : - ${watchersCount}"
        binding.tvOpenIssuesCount.text = "Open issues count : - ${openIssuesCount}"
        binding.tvVisibility.text = "Visibility : - ${visibility}"


    }
    override fun showProgress() {
    }
    override fun hideProgress() {
    }
}